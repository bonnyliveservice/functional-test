var webdriver = require('selenium-webdriver');

// Input capabilities
var capabilities = {
    'os' : 'OS X',
    'os_version' : 'Sierra',
    'browserName' : 'Chrome',
    'browser_version' : '71.0 beta',
    'browserstack.local' : 'false',
    'browserstack.selenium_version' : '3.5.2',
    'browserstack.user' : process.env.BROWSERSTACK_USER,
    'browserstack.key' : process.env.BROWSERSTACK_KEY
}

var driver = new webdriver.Builder().
  usingServer('http://hub-cloud.browserstack.com/wd/hub').
  withCapabilities(capabilities).
  build();

module.exports = driver
