import assert from 'assert'
import driver from '../driver'
import webdriver from 'selenium-webdriver'

const domain = process.env.DOMAIN
const account = process.env.ACCOUNT
const userName = process.env.USERNAME
let email = 'TestRobot' + Math.floor(Math.random() * 100) + Math.floor(Math.random() * 999) + '@register.com';
const phone = process.env.PHONE
const password = process.env.PASSWORD

// let registerData = {
//     userName: userName,
//     email: email,
//     phone: phone,
//     password: password,
//     AuthPassword: password,
//     occupation:['醫療'],
//     badminton_level:['高手'],
//     hobby:1
//     //   occupation
// }

let allTestCase = [{
    userName: userName,
    email: email,
    phone: phone,
    password: password,
    AuthPassword: password,
    occupation: '醫療',
    badminton_level: '高手',
    // hobby: 1,
    // alertMsg
}, {
    userName: "",
    email: email,
    phone: phone,
    password: password,
    AuthPassword: password,
    occupation: '醫療',
    badminton_level: '高手',
    // hobby: 1,
    // alertMsg
}]



const register = class {
    constructor() {
        this['name'] = 'register'
    }


    async allTestCase() {

        try {
            await this.nameFail()
            // clear
            const alertMsg1 = await driver.switchTo().alert().getText()
            assert.equal(alertMsg1, '產生資料失敗，欠缺必填項目或是格式不符');

            driver.switchTo().alert().accept();
            // alert 點選確定
            console.log("name fail");


            await this.emailNullFail()
            // clear
            // 他會 refresh, so it is ok
            const alertMsg2 = await driver.switchTo().alert().getText()
            assert.equal(alertMsg2, 'E-MAIL不得為空或格式錯誤');

            driver.switchTo().alert().accept();
            // driver.switchTo().alert().accept();

            const alertMsg3 = await driver.switchTo().alert().getText()
            assert.equal(alertMsg3, '請閱讀並接受個資保護聲明');
            driver.switchTo().alert().accept();
            // alert 點選確定
            console.log("email fail");
            // checkbox 不能重複勾

            // allTestCase.map((e)=>{
            //     this.registerModule(e)
            // })

            await this.success()
            // .then(() => {
            console.log("success will ");
            const alertMsg = await driver.switchTo().alert().getText()
            setTimeout(() => {
                assert.equal(alertMsg, '註冊成功');
            }, 5000)
            // })

        } catch (e) {
            console.error('[Error]', this.name + 'failed', e)
            await driver.quit();
        } finally {
            console.log('[Info]', this.name + 'completed')
            await driver.quit();
        }
    }

    async success() {
        /**
            測試項目：註冊成功
        */
        let registerData = {
            userName: userName,
            email: email,
            phone: phone,
            password: password,
            AuthPassword: password,
            occupation: '醫療',
            badminton_level: '高手',
            // hobby: 1,
            // alertMsg
        }
        await this.registerModule(registerData)

    }


    async nameFail() {
        /**
         測試項目：註冊失敗 / 測試不輸入 name
         */

        let registerData = {
            userName: "",
            email: email,
            phone: phone,
            password: password,
            AuthPassword: password,
            occupation: '醫療',
            badminton_level: '高手',
            // hobby: 1,
            // alertMsg
        }

        await this.registerModule(registerData)
        // const alertMsg = await driver.switchTo().alert().getText()
        // assert.equal(alertMsg, '姓名不得為空');
    }

    async emailNullFail() {
        /**
            測試項目：註冊失敗 / email為空
        */
        let registerData = {
            userName: userName,
            email: "",
            phone: phone,
            password: password,
            AuthPassword: password,
            occupation: '醫療',
            badminton_level: '高手',
            // hobby: 1,
            // alertMsg
        }

        // 丟不同參數
        await this.registerModule(registerData)
        // .then 其他錯誤
        // 結果呢?
        // const alertMsg = await driver.switchTo().alert().getText()
        //     assert.equal(alertMsg, 'E-MAIL不得為空或格式錯誤');
    }
    async emailWrongFail() {
        /**
           測試項目：註冊失敗 / 測試輸入格式不正確的 Email
         */
        let registerData = {
            userName: userName,
            email: 'sunnytest.com',
            phone: phone,
            password: password,
            AuthPassword: password,
            occupation: '醫療',
            badminton_level: '高手',
            // hobby: 1,
            // alertMsg
        }

        this.registerModule(registerData)
        // const alertMsg = await driver.switchTo().alert().getText()
        //     assert.equal(alertMsg, 'E-MAIL不得為空或格式錯誤');
    }

    async phoneFail() {
        /**
           測試項目：測試項目：登入失敗 / 測試不輸入 phone
         */
        let registerData = {
            userName: userName,
            email: email,
            phone: "",
            password: password,
            AuthPassword: password,
            occupation: '醫療',
            badminton_level: '高手',
            // hobby: 1,
            // alertMsg
        }

        this.registerModule(registerData)
        // const alertMsg = await driver.switchTo().alert().getText()
        // assert.equal(alertMsg, '電話不得為空');
    }

    async passwordFail() {
        /**
           測試項目：測試項目：登入失敗 / 測試不輸入 password
         */
        let registerData = {
            userName: userName,
            email: email,
            phone: phone,
            password: "",
            AuthPassword: password,
            occupation: '醫療',
            badminton_level: '高手',
            // hobby: 1,
            // alertMsg
        }
        this.registerModule(registerData)
        // const alertMsg = await driver.switchTo().alert().getText()
        // assert.equal(alertMsg, '密碼不得為空');
    }


    // 測試模組 結果在 allTestCase 檢視符不符合
    async registerModule(registerData) {

        console.log(registerData);

        setTimeout(() => { driver.quit }, 40000)
        await driver.get(domain + '/register');

        await driver.findElement(webdriver.By.className('form-control'))
            .findElement(webdriver.By.name('name')).sendKeys(registerData.userName);
        await driver.findElement(webdriver.By.name('email'))
            .sendKeys(registerData.email);
        await driver.findElement(webdriver.By.name('phone'))
            .sendKeys(registerData.phone);
        await driver.findElement(webdriver.By.name('password'))
            .sendKeys(registerData.password);
        await driver.findElement(webdriver.By.name('AuthPassword'))
            .sendKeys(registerData.password);

        // this.executeScript( script(function or string), want to send to script )

        await driver.findElement(webdriver.By.name('occupation'))
            .sendKeys(registerData.occupation);
        // .selectByIndex(2)
        await driver.findElement(webdriver.By.name('badminton_level'))
            .sendKeys(registerData.badminton_level)
        await driver.findElement(webdriver.By.js(() => {
            let hobbyArray = document.getElementsByName('hobby')
            return hobbyArray[1]
            // (registerData.hobby).map(e =>{ hobbyArray[e].click()})
        })).click()

        // 是一個 object 
        // console.log(" hobbyButtons:  ", hobbyButtons )
        // , JSON.stringify(hobbyButtons));
        // console.log( Object.values(driver.findElements(webdriver.By.name('hobby'))) )


        await driver.findElement(webdriver.By.className('ckbox')).click();
        await driver.findElement(webdriver.By.className('main-form form'))
            .findElement(webdriver.By.tagName('button')).click()

    }






    /**
    以下是 需要調整的 目前是 dead code
    */


    async test() {

        /**
           測試項目：測試項目：登入失敗 / 測試不輸入 AuthPassword
         */

        try {
            // open register page
            await driver.get(domain + '/register');
            await driver.findElement(webdriver.By.name('name')).sendKeys(userName);
            await driver.findElement(webdriver.By.name('email')).sendKeys(email);
            await driver.findElement(webdriver.By.name('phone')).sendKeys(phone);
            await driver.findElement(webdriver.By.name('password')).sendKeys(password);
            await driver.findElement(webdriver.By.name('AuthPassword')).sendKeys('');
            await driver.findElement(webdriver.By.name('occupation').val('1')).click();
            await driver.findElement(webdriver.By.name('badminton_level').val('1')).click();
            await driver.findElement(webdriver.By.name('hobby').val('1')).click();
            await driver.findElement(webdriver.By.className('ckbox')).click();
            await driver.findElement(webdriver.By.className('main-form').children('button')).click();
            // get alert text
            const alertMsg = await driver.switchTo().alert().getText()
            assert.equal(alertMsg, '密碼驗證不得為空');
        } catch (e) {
            console.error('[Error]', this.name + '.success', 'failed', e)
            await driver.quit();
        } finally {
            console.log('[Info]', this.name + '.success', 'completed')
            await driver.quit();
        }
    }

    async test() {

        /**
           測試項目：測試項目：登入失敗 / 測試輸入不相同的 password
         */

        try {
            // open register page
            await driver.get(domain + '/register');
            await driver.findElement(webdriver.By.name('name')).sendKeys(userName);
            await driver.findElement(webdriver.By.name('email')).sendKeys(email);
            await driver.findElement(webdriver.By.name('phone')).sendKeys(phone);
            await driver.findElement(webdriver.By.name('password')).sendKeys(password);
            await driver.findElement(webdriver.By.name('AuthPassword')).sendKeys('a87654321');
            await driver.findElement(webdriver.By.name('occupation').val('1')).click();
            await driver.findElement(webdriver.By.name('badminton_level').val('1')).click();
            await driver.findElement(webdriver.By.name('hobby').val('1')).click();
            await driver.findElement(webdriver.By.className('ckbox')).click();
            await driver.findElement(webdriver.By.className('main-form').children('button')).click();
            // get alert text
            const alertMsg = await driver.switchTo().alert().getText()
            assert.equal(alertMsg, '密碼確認錯誤');
        } catch (e) {
            console.error('[Error]', this.name + '.success', 'failed', e)
            await driver.quit();
        } finally {
            console.log('[Info]', this.name + '.success', 'completed')
            await driver.quit();
        }
    }

    async test() {

        /**
           測試項目：測試項目：登入失敗 / 測試不勾選 checkbox
         */

        try {
            // open register page
            await driver.get(domain + '/register');
            await driver.findElement(webdriver.By.name('name')).sendKeys(userName);
            await driver.findElement(webdriver.By.name('email')).sendKeys(email);
            await driver.findElement(webdriver.By.name('phone')).sendKeys(phone);
            await driver.findElement(webdriver.By.name('password')).sendKeys(password);
            await driver.findElement(webdriver.By.name('AuthPassword')).sendKeys(password);
            await driver.findElement(webdriver.By.name('occupation').val('1')).click();
            await driver.findElement(webdriver.By.name('badminton_level').val('1')).click();
            await driver.findElement(webdriver.By.name('hobby').val('1')).click();
            // 不勾選 checkbox
            // await driver.findElement(webdriver.By.className('ckbox')).click();
            await driver.findElement(webdriver.By.className('main-form').children('button')).click();
            // get alert text
            const alertMsg = await driver.switchTo().alert().getText()
            assert.equal(alertMsg, '請閱讀並接受個資保護聲明');
        } catch (e) {
            console.error('[Error]', this.name + '.success', 'failed', e)
            await driver.quit();
        } finally {
            console.log('[Info]', this.name + '.success', 'completed')
            await driver.quit();
        }
    }

    async failed() {

        /**
           測試項目：登入失敗 / Email 已經註冊
         */

        try {
            // open register page
            await driver.get(domain + '/register');
            await driver.findElement(webdriver.By.name('name')).sendKeys(userName);
            await driver.findElement(webdriver.By.name('email')).sendKeys(account);
            await driver.findElement(webdriver.By.name('phone')).sendKeys(phone);
            await driver.findElement(webdriver.By.name('password')).sendKeys(password);
            await driver.findElement(webdriver.By.name('AuthPassword')).sendKeys(password);
            await driver.findElement(webdriver.By.name('occupation').val('1')).click();
            await driver.findElement(webdriver.By.name('badminton_level').val('1')).click();
            await driver.findElement(webdriver.By.name('hobby').children("option").each(function () { $(this).val() })).click();
            await driver.findElement(webdriver.By.className('ckbox')).click();
            await driver.findElement(webdriver.By.className('main-form').children('button')).click();
            // get alert text
            const alertMsg = await driver.switchTo().alert().getText()
            assert.equal(alertMsg, '信箱已被使用');
        } catch (e) {
            console.error('[Error]', this.name + '.success', 'failed', e)
            await driver.quit();
        } finally {
            console.log('[Info]', this.name + '.success', 'completed')
            await driver.quit();
        }
    }
}

export default new register()