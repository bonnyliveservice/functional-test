import assert from 'assert'
import driver from '../driver'
import webdriver from 'selenium-webdriver'

const domain = process.env.DOMAIN
const account = process.env.ACCOUNT
const password = process.env.PASSWORD

const Login = class {
  constructor () {
    this['name'] = 'Login'
  }

  async success () {
    
    /**
       測試項目：登入成功
     */

    try {
        // open login page
        await driver.get(domain + '/Login');
        await driver.findElement(webdriver.By.name('account')).sendKeys(account)
        await driver.findElement(webdriver.By.name('password')).sendKeys(password);
        await driver.findElement(webdriver.By.className('vYEHV')).click();
        // get URL
        const URL = await driver.getCurrentUrl();
        assert.equal(URL, domain + '/myInfo');
    } catch(e) {
        console.error('[Error]', this.name + '.success', 'failed', e)
        await driver.quit();
    } finally {
        console.log('[Info]', this.name + '.success', 'completed')
        await driver.quit();
    }
  }

  async test () {

    /**
       測試項目：登入失敗 / 測試輸入格式不正確的 Email
     */

    try {
        // open login page
        await driver.get(domain + '/Login');
        await driver.findElement(webdriver.By.name('account')).sendKeys('sunnytest.com')
        await driver.findElement(webdriver.By.name('password')).sendKeys(password);
        await driver.findElement(webdriver.By.className('vYEHV')).click();
        // get alert text
        const alertMsg = await driver.switchTo().alert().getText()
        assert.equal(alertMsg, '請輸入正確的E-Mail帳號');
    } catch(e) {
        console.error('[Error]', this.name + '.test', 'failed', e)
        await driver.quit();
    } finally {
        console.log('[Info]', this.name + '.test', 'completed')
        await driver.quit();
    }
  }

  async failed () {

    /**
       測試項目：登入失敗 / 帳密錯誤
     */

    try {
        // open login page
        await driver.get(domain + '/Login');
        await driver.findElement(webdriver.By.name('account')).sendKeys(account)
        await driver.findElement(webdriver.By.name('password')).sendKeys('12345678');
        await driver.findElement(webdriver.By.className('vYEHV')).click();
        // get alert text
        const alertMsg = await driver.switchTo().alert().getText()
        assert.equal(alertMsg, '帳號密碼有誤');
    } catch(e) {
        console.error('[Error]', this.name + '.failed', 'failed', e)
        await driver.quit();
    } finally {
        console.log('[Info]', this.name + '.failed', 'completed')
        await driver.quit();
    }
  }
}

export default new Login()